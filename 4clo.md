#### ---Please refer to the 4clojure problems page for questions---

## Problem 1

```
true
```

## Problem 2

```
4
```

## Problem 3

```
"HELLO WORLD"
```

## Problem 4

```
:a :b :c
```
## Problem 5

```
'(1 2 3 4)
```

## Problem 6

```
:a :b :c
```

## Problem 7

```
[1 2 3 4]
```

## Problem 8

```
#{:a :b :c :d}
```

## Problem 9

```
2
```

## Problem 10

```
20
```

## Problem 11

```
{:b 2}
```

## Problem 12

```
3
```

## Problem 13

```
[20 30 40]
```

## Problem 14

```
8
```

## Problem 15

```
* 2
```

## Problem 16

```
(fn [name] (str "Hello, " name "!"))
```

## Problem 17

```
'(6 7 8)
```

## Problem 18

```
'(6 7)
```

## Problem 19

```
(fn [x] (nth x (- (count x) 1)))
```

## Problem 20

```
(fn [x] (nth x (- (count x) 2)))
```

## Problem 21

```
(fn [a b](get (vec a) b))
```


## Problem 22

```
(fn [x](reduce (fn [x _] (inc x)) 0  x))
```

## Problem 23

```
(fn [x](into () x))
```


## Problem 24

```
reduce +
```

## Problem 25

```
(fn [x] (filter odd? x))
```

## Problem 25

```
(fn [x] (filter odd? x))
```

## Problem 26

```
(fn [x](take x (map first (iterate (fn [[a b]] [b (+ a b)]) [1 1]))))
```

## Problem 27

```
(fn [x] (= (seq x) (reverse (seq x))))
```

## Problem 28

```
(fn flatn [x]
  (let [current (first x) remaining (next x)]
    (concat
     (if (sequential? current)
        (flatn current)
        [current])
     (when (sequential? remaining)
        (flatn remaining)))))
```

## Problem 29

```
(fn [x] (reduce str (filter #(Character/isUpperCase %) x)))
```

## Problem 30

```
(fn [x]
 (reduce (fn [acc e] (if(= e (last acc))
                       acc
                       (conj acc e))) [] x))
```

## Problem 31

```
(fn [x] (partition-by identity x))
```

## Problem 32

```
(fn [x] (reduce (fn [acc e] (into acc [e e])) [] (vec x)))
```

## Problem 33

```
(fn [coll n] (reduce (fn [accum e](into accum e)) [] (map (fn [x] (repeat n x)) coll)))
```

## Problem 34

```
(fn [x y] (take (- y x) (iterate #(inc %) x)))
```

## Problem 35

```
7
```

## Problem 36

```
[x 7
 y 3
 z 1]
```

## Problem 37

```
"ABC"
```

## Problem 38

```
(fn [arg1 & remaining]
  (reduce (fn [maks current] (if (< maks current) current maks)) arg1 remaining))
```

## Problem 39

```
(fn [seq1 seq2] (reduce (fn [acc e] (into acc e)) []  (map (fn [x y] [x y]) seq1 seq2)))
```

## Problem 40

```
(fn [e coll] (reduce (fn [acc x] (if (= x (last coll))
                                   (into acc [x])
                                   (into acc [x e]))) [] coll))
```

## Problem 41

```
(fn [seq1 n]
  (mapcat #(take (dec n) %) (partition-all n seq1)))
```

## Problem 42

```
(fn [n] (reduce * (take n (iterate inc 1))))
```

## Problem 43

```
(fn [coll n]
  (apply map list (partition n coll)))
```

## Problem 44

```
(fn [n coll] (if (pos? n)
               (take-last (count coll) (flatten (conj (vec coll) (take (mod n (count coll)) coll))))
               (take (count coll) (flatten (conj  (seq coll) (take-last (mod (java.lang.Math/abs n) (count coll)) coll))))))
```

## Problem 45

```
'(1 4 7 10 13)
```

## Problem 46

```
(fn [op] (fn [arg1 arg2] (op arg2 arg1)))
```

## Problem 47

```
4
```

## Problem 48

```
6
```

## Problem 49

```
(fn [n coll] (conj (conj () (take-last (- (count coll) n) coll))))
```

## Problem 50

```
(fn [coll] (vals (group-by type coll)))
```
